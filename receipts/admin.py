from django.contrib import admin
from receipts.models import Account, ExpenseCategory, Receipt

admin.site.register(Account)
admin.site.register(ExpenseCategory)
admin.site.register(Receipt)
