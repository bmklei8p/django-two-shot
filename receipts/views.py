from django.shortcuts import redirect
from django.views.generic import ListView, CreateView
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy


class ReceiptsListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "list.html"
    context_object_name = "receipts_list"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "expense_list.html"
    context_object_name = "expense_list"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "account_list.html"
    context_object_name = "account_list"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)


class ReceiptsCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = "create.html"
    fields = ["vendor", "total", "tax", "date", "category", "account"]
    success_url = reverse_lazy("home")

    def form_valid(self, form):
        form = form.save(commit=False)
        form.purchaser = self.request.user
        form.save()
        return redirect("home")


class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = "expense_create.html"
    fields = ["name"]

    def form_valid(self, form):
        form = form.save(commit=False)
        form.owner = self.request.user
        form.save()
        return redirect("list_categories")


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    template_name = "account_create.html"
    fields = ["name", "number"]

    def form_valid(self, form):
        form = form.save(commit=False)
        form.owner = self.request.user
        form.save()
        return redirect("list_accounts")
